import React from 'react';
import { Button } from '@mui/material';
// @ts-ignore
import { SimpleReactDialog } from 'simple-react-dialog';
import styled from 'styled-components';
import type { IEmployee } from '../../utils/interfaces';
import CreateEmployeeForm from '../create-employee/CreateEmployeeForm';

const Card = styled.div`
  background-color: white;
  display: flex;
  flex-direction: column;
  padding: 16px 24px;
`;

interface IProp {
  setIsOpen: any;
  isOpen: boolean;
  createEmployee: (arg1: IEmployee, arg2: any) => void;
}

const NewEmployeeDialog: React.FC<IProp> = ({ createEmployee, isOpen, setIsOpen }) => {
  return (
    <div>
      <Button type="button" variant="contained" onClick={() => setIsOpen(true)}>
        Add new employee
      </Button>
      <SimpleReactDialog onClose={() => setIsOpen(false)} value={isOpen}>
        <Card>
          <h2>Create New Employee</h2>
          <CreateEmployeeForm onSubmit={createEmployee} />
        </Card>
      </SimpleReactDialog>
    </div>
  );
};

export default NewEmployeeDialog;
