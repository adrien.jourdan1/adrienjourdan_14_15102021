import React from 'react';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import type { IEmployee } from '../../utils/interfaces';
import employeeFixtures from '../../utils/fixtures';

interface IProp {
  employees: IEmployee[];
}

const EmployeeListTable: React.FC<IProp> = ({ employees }) => {
  const columns: GridColDef[] = [
    { field: 'firstname', headerName: 'Firstname', width: 150 },
    { field: 'lastname', headerName: 'Lastname', width: 150 },
    { field: 'birthdate', headerName: 'Birthdate', width: 150 },
    { field: 'startDate', headerName: 'Start Date', width: 150 },
    { field: 'adress', headerName: 'Adress', width: 150 },
    { field: 'city', headerName: 'City', width: 150 },
    { field: 'state', headerName: 'State', width: 150 },
    { field: 'zipCode', headerName: 'Zip Code', width: 150 },
    { field: 'department', headerName: 'Department', width: 150 },
  ];

  const test = [...employees, ...employeeFixtures];

  return (
    <Box
      sx={{
        height: '576px',
        width: '100%',
      }}
    >
      <DataGrid columns={columns} rows={test} disableSelectionOnClick />
    </Box>
  );
};

export default EmployeeListTable;
