/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import { TextField, Button, MenuItem, Select, Box } from '@mui/material';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import MobileDatePicker from '@mui/lab/MobileDatePicker';
import { useHistory } from 'react-router-dom';
import americanStates from '../../utils/states.utils';
import type { IEmployee } from '../../utils/interfaces';

interface IProp {
  onSubmit: (arg1: IEmployee, arg2: any) => void;
}

const CreateEmployeeForm: React.FC<IProp> = ({ onSubmit }) => {
  const departments = [
    { value: 'sales', text: 'Sales' },
    { value: 'engineering', text: 'Engineering' },
    { value: 'hr', text: 'Human Resources' },
    { value: 'legal', text: 'Legal' },
  ];

  const history = useHistory();

  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [birthdate, setBirthdate] = useState(new Date('1980-01-01').toLocaleDateString('fr-FR'));
  const [startDate, setStartDate] = useState(new Date().toLocaleDateString('fr-FR'));
  const [adress, setAdress] = useState('');
  const [city, setCity] = useState('');
  const [state, setState] = useState(americanStates[0].value);
  const [zipCode, setZipCode] = useState('');
  const [department, setDepartment] = useState(departments[0].value);

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    onSubmit(
      {
        id: Math.random().toString(36).substr(2, 9),
        firstname,
        lastname,
        birthdate,
        startDate,
        adress,
        city,
        state,
        zipCode,
        department,
      },
      history,
    );
  };

  const changeBirthdate = (date: Date | null) => {
    if (!date) {
      setBirthdate('');
      return;
    }

    const dateString = date.toLocaleDateString('en-US');
    setBirthdate(dateString);
  };

  const changeStartdate = (date: Date | null) => {
    console.log(date);
    if (!date) {
      setStartDate('');
      return;
    }

    const dateString = date.toLocaleDateString('en-US');
    setStartDate(dateString);
  };

  return (
    <Box
      component="form"
      sx={{
        '& .MuiTextField-root, .select, .adress, .MuiButton-root': { my: 1 },
        '& .adress': { borderRadius: '4px' },
      }}
      onSubmit={handleSubmit}
    >
      <TextField
        label="Firstname"
        value={firstname}
        onChange={(e) => setFirstname(e.target.value)}
        fullWidth
        required
      />
      <TextField
        label="Lastname"
        value={lastname}
        onChange={(e) => setLastname(e.target.value)}
        fullWidth
        required
      />
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <MobileDatePicker
          label="Birthdate"
          value={birthdate}
          openTo="year"
          onChange={changeBirthdate}
          renderInput={(params) => <TextField fullWidth required {...params} />}
          inputFormat="MM/dd/yyyy"
          disableFuture
        />
        <MobileDatePicker
          label="Start date"
          value={startDate}
          onChange={changeStartdate}
          renderInput={(params) => <TextField fullWidth required {...params} />}
        />
      </LocalizationProvider>
      <fieldset className="adress">
        <legend>Address</legend>
        <TextField
          label="Adress"
          value={adress}
          onChange={(e) => setAdress(e.target.value)}
          fullWidth
          required
        />
        <TextField
          label="City"
          value={city}
          onChange={(e) => setCity(e.target.value)}
          fullWidth
          required
        />
        <Select
          className="select"
          value={state}
          onChange={(e) => setState(e.target.value)}
          MenuProps={{ PaperProps: { sx: { maxHeight: 250 } } }}
          fullWidth
          required
        >
          {americanStates.map((s) => (
            <MenuItem key={s.value} value={s.value}>
              {s.text}
            </MenuItem>
          ))}
        </Select>
        <TextField
          label="Zip code"
          value={zipCode}
          onChange={(e) => setZipCode(e.target.value)}
          fullWidth
          required
        />
      </fieldset>
      <Select
        className="select"
        value={department}
        onChange={(e) => setDepartment(e.target.value)}
        fullWidth
        required
        autoWidth
      >
        {departments.map((d) => (
          <MenuItem key={d.value} value={d.value}>
            {d.text}
          </MenuItem>
        ))}
      </Select>
      <Button type="submit" variant="contained" fullWidth>
        Create Employee
      </Button>
    </Box>
  );
};

export default CreateEmployeeForm;
