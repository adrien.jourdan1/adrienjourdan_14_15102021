import React, { lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';
import './utils/style/global.scss';

const App = lazy(() => import('./App'));

const renderLoader = () => <p>Loading</p>;

ReactDOM.render(
  <Suspense fallback={renderLoader()}>
    <App />
  </Suspense>,
  document.getElementById('root'),
);
