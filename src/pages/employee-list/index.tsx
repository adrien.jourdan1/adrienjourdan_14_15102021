import React, { useState, lazy } from 'react';
import { StylesProvider } from '@material-ui/styles';
import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import Toast from '@mui/material/Snackbar';
import type { IEmployee } from '../../utils/interfaces';

const EmployeeListTable = lazy(() => import('../../components/employee-list/EmployeeListTable'));
const NewEmployeeDialog = lazy(() => import('../../components/employee-list/NewEmployeeDialog'));

const EmployeeList: React.FC = () => {
  const [employees, setEmployees] = useState([] as IEmployee[]);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isToastOpen, setIsToastOpen] = useState(false);

  const createEmployee = (employee: IEmployee) => {
    setEmployees([...employees, employee]);
    setIsDialogOpen(false);
    setIsToastOpen(true);
  };

  return (
    <main>
      <StylesProvider injectFirst>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: '24px',
            flexWrap: 'wrap',
          }}
        >
          <h1>Current Employees</h1>
          <NewEmployeeDialog
            createEmployee={createEmployee}
            isOpen={isDialogOpen}
            setIsOpen={setIsDialogOpen}
          />
        </Box>
        <EmployeeListTable employees={employees} />
        <Toast open={isToastOpen} autoHideDuration={6000} onClose={() => setIsToastOpen(false)}>
          <Alert onClose={() => setIsToastOpen(false)} severity="success" sx={{ width: '100%' }}>
            New employee successfully added !
          </Alert>
        </Toast>
      </StylesProvider>
    </main>
  );
};

export default EmployeeList;
