import React, { lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

const EmployeeList = lazy(() => import('./pages/employee-list'));

const App: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <EmployeeList />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
