export interface IEmployee {
  id: string;
  firstname: string;
  lastname: string;
  birthdate: string;
  startDate: string;
  adress: string;
  city: string;
  state: string;
  zipCode: string;
  department: string;
}
